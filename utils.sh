#install vim
sudo apt-get install vim -y --fix-missing

#install pip
sudo apt-get install python-pip -y --fix-missing

#install python-MySQLdb for torndb
sudo apt-get install python-MySQLdb -y

#install tornado:the latest version
sudo apt-get install tornado

#install torndb using pip tool
sudo apt-get install torndb

#install sublime-text-2
sudo add-apt-repository ppa:webupd8team/sublime-text-2 -y
sudo apt-get update
sudo apt-get install sublime-text -y

#install nodejs: the latest version
sudo apt-get install python-software-properties -y
sudo apt-add-repository ppa:chris-lea/node.js
sudo apt-get update
sudo apt-get install nodejs -y --fix-missing

