yum install curl-devel expat-devel gettext-devel openssl-devel zlib-devel gcc perl-ExtUtils -y
yum install perl-devel perl-CPAN -y

cd /usr/src
wget https://www.kernel.org/pub/software/scm/git/git-2.0.1.tar.gz
tar xzf git-2.0.1.tar.gz
cd git-2.0.1
make prefix=/usr/local/git all
make prefix=/usr/local/git install
echo "export PATH=$PATH:/usr/local/git/bin" >> /etc/bashrc
source /etc/bashrc


